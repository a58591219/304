var express = require('express');
var mongoose = require('mongoose');
const {MongoClient} = require('mongodb');
var path = require('path');

var app = express();

var getusername = "";
var getuserid = "";
var getuseremail = "";
var getuserpassword = "";
var getusertype = "";

app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname,'.')));

app.get('/',function(req,res){
    //var bookdata = "";
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function findAll(client) {
        const result = await client.db("book_data").collection("listbook").find({}, { projection: { _id: 0, bookname: 1, bookauthor: 1,bookdate: 1 } }).toArray(function(err, result) {
            if (err) throw err;
            var getbookauthor = "";
            var getbookname = "";
            var getbookdate = "";
            result.forEach(element => {
                console.log(element);
                getbookauthor += element.bookauthor+"####@@@@";
                getbookname += element.bookname+"####@@@@";
                getbookdate += element.bookdate+"####@@@@";
            });
            getbookauthor = "<script>var bookauthor = '"+getbookauthor+"'</script>";
            getbookname = "<script>var bookname = '"+getbookname+"'</script>";
            getbookdate = "<script>var bookdate = '"+getbookdate+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('main.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(getbookname+getbookauthor+getbookdate+title+data);
            res.end();
    });
          });
    }
})

app.get('/registered',function(req,res){
    res.sendFile('registered.html', { root: '.' })
})

app.get('/tick',function(req,res){
    res.sendFile('tick.html', { root: '.' })
})

app.get('/new',function(req,res){
    if(getusertype=="admin"){
        fs = require('fs')
    fs.readFile('new.html', 'utf8', function (err,data) {
       if (err) {
          return console.log(err);
       }
      //res.sendFile('main.html', { root: '.' })
    res.writeHead(200,{'Content-Type':'text/html'});
    res.write("<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>"+data);
    res.end();
    });
    }else{
        res.redirect('/');
    }
})

app.post('/donew',function(req,res){
    var bookname = req.body.name;
    var bookauthor = req.body.author;
    var bookdate = req.body.date;
    var bookintroduction = req.body.introduction;
    var bookprice = req.body.price
    async function main() {
        const uri = 'mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
        
            //await listDatabases(client);
    
        
                await createListing(client,
                    {
                        bookname: bookname,
                        bookauthor: bookauthor,
                        bookdate: bookdate,
                        bookintroduction: bookintroduction,
                        bookstatus: "Y",
                        bookprice: bookprice
                    }
                );
        
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function createListing(client, newListing){
        const result = await client.db("book_data").collection("listbook").insertOne(newListing);
        console.log(`New listing created with the following id: ${result.insertedId}`);
        res.redirect('/');
    }
})

app.get('/favorite',function(req,res){
    //var bookdata = "";
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function findAll(client) {
        const result = await client.db("favorite_data").collection("listfavorite").find({useremail: getuseremail}).toArray(function(err, result) {
            if (err) throw err;
            var getbookname = "";
            var getadddate = "";
            result.forEach(element => {
                console.log(element);
                getbookname += element.bookname+"####@@@@";
                getadddate += element.adddate+"####@@@@";
            });
            getbookname = "<script>var bookname = '"+getbookname+"'</script>";
            getadddate = "<script>var bookdate = '"+getadddate+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('favorite.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(getbookname+getadddate+title+data);
            res.end();
    });
          });
    }
})

app.get('/findfavoritebook',function(req,res){
    var getname = req.query.bookname;
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function findAll(client) {                                   //$or: [ {bookname: new RegExp(getname)}, {bookauthor: new RegExp(getname)} ]
        const result = await client.db("favorite_data").collection("listfavorite").find({bookname: new RegExp(getname),useremail: getuseremail}).toArray(function(err, result) {
            if (err) throw err;
            var getbookname = "";
            var getbookdate = "";
            result.forEach(element => {
                console.log(element);
                getbookname += element.bookname+"####@@@@";
                getbookdate += element.adddate+"####@@@@";
            });
            getbookname = "<script>var bookname = '"+getbookname+"'</script>";
            getbookdate = "<script>var bookdate = '"+getbookdate+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('favorite.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(getbookname+getbookdate+title+data);
            res.end();
    });
          });
    }
})

app.get('/login',function(req,res){
    res.sendFile('login.html', { root: '.' })
})

app.post('/douserchange',function(req,res){
    var changename = req.body.name;
    var changepassword = req.body.password;

    getusername = changename;
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();

            if(changepassword!=""){
               await updateListingByName(client, getuseremail, { password: changepassword, name: changename});
            }else{
                await updateListingByName(client, getuseremail, { name: changename});
            }
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function updateListingByName(client, nameOfListing, updatedListing) {
        const result = await client.db("user_data").collection("listuser")
                            .updateOne({ email: nameOfListing }, { $set: updatedListing });
        console.log(`${result.matchedCount} document(s) matched the query criteria.`);
        console.log(`${result.modifiedCount} document(s) was/were updated.`);
        res.redirect('/');
    }
})

app.post('/dobookchange',function(req,res){
    var changename = req.body.cname;
    var changeintroduction = req.body.cintroduction;
    var changeprice = req.body.cprice;

    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();

            await updateListingByName(client, changename, { bookintroduction: changeintroduction,bookprice: changeprice});
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function updateListingByName(client, nameOfListing, updatedListing) {
        const result = await client.db("book_data").collection("listbook")
                            .updateOne({ bookname: nameOfListing }, { $set: updatedListing });
        console.log(`${result.matchedCount} document(s) matched the query criteria.`);
        console.log(`${result.modifiedCount} document(s) was/were updated.`);
        res.redirect('/');
    }
})

app.get('/user',function(req,res){
    if(getusername==""){
        res.redirect('/');
    }else{
    fs = require('fs')
    fs.readFile('user.html', 'utf8', function (err,data) {
       if (err) {
          return console.log(err);
       }
      //res.sendFile('main.html', { root: '.' })
    res.writeHead(200,{'Content-Type':'text/html'});
    res.write("<script>var usertype='"+getusertype+"';var name='"+getusername+"';var email='"+getuseremail+"';var id='"+getuserid+"';</script>"+data);
    res.end();
    });
}
})

app.get('/logout',function(req,res){
    getusername = "";
    getuserid = "";
    getuseremail = "";
    getuserpassword = "";
    getusertype = "";
    res.redirect('/');
})

app.get('/findbook',function(req,res){
    var getname = req.query.bookname;
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function findAll(client) {                                   //$or: [ {bookname: new RegExp(getname)}, {bookauthor: new RegExp(getname)} ]
        const result = await client.db("book_data").collection("listbook").find({$or: [ {bookname: new RegExp(getname)}, {bookauthor: new RegExp(getname)} ]}, { projection: { _id: 0, bookname: 1, bookauthor: 1,bookdate: 1 } }).toArray(function(err, result) {
            if (err) throw err;
            var getbookauthor = "";
            var getbookname = "";
            var getbookdate = "";
            result.forEach(element => {
                console.log(element);
                getbookauthor += element.bookauthor+"####@@@@";
                getbookname += element.bookname+"####@@@@";
                getbookdate += element.bookdate+"####@@@@";
            });
            getbookauthor = "<script>var bookauthor = '"+getbookauthor+"'</script>";
            getbookname = "<script>var bookname = '"+getbookname+"'</script>";
            getbookdate = "<script>var bookdate = '"+getbookdate+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('main.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(getbookname+getbookauthor+getbookdate+title+data);
            res.end();
    });
          });
    }
})

app.get('/bookdata',function(req,res){
    var getname = req.query.bookname;

    var getbookauthor = "";
    var getbookname = "";
    var getbookdate = "";
    var getbookintroduction = "";
    var getbookstatus = "";
    var getbookprice = "";
    var title = "";
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);

        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function findAll(client) {
        const result = await client.db("book_data").collection("listbook").find({bookname: getname}).toArray(function(err, result) {
            if (err) throw err;
            result.forEach(element => {
                console.log(element);
                getbookauthor = element.bookauthor;
                getbookname = element.bookname;
                getbookdate = element.bookdate;
                getbookintroduction = element.bookintroduction;
                getbookstatus = element.bookstatus;
                getbookprice = element.bookprice;
            });
            //console.log(getbookname);
            if(result!=""){
               res.redirect("/bookdata1?getbookauthor="+getbookauthor+"&getbookname="+getbookname+"&getbookdate="+getbookdate+"&getbookintroduction="+getbookintroduction+"&getbookstatus="+getbookstatus+"&getbookprice="+getbookprice);
            }else{
                res.redirect("/");
            }
        });
    }
})

app.get('/bookdata1',function(req,res){
    var getbookstatus = req.query.getbookstatus;
    var getbookauthor = req.query.getbookauthor;
    var getbookdate = req.query.getbookdate;
    var getbookname = req.query.getbookname;
    var getbookintroduction = req.query.getbookintroduction;
    var getbookprice = req.query.getbookprice;

    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    main().catch(console.error);

    async function findAll(client) {
        const result = await client.db("favorite_data").collection("listfavorite").find({bookname: getbookname,useremail: getuseremail}, {}).toArray(function(err, result) {
            if (err) throw err;
            var fav = "<script>var fav = 'N'</script>";
            if(result!=""){
                fav = "<script>var fav = 'Y'</script>";
            }
            getbookstatus = "<script>var bookstatus = \""+getbookstatus+"\"</script>";
            getbookauthor = "<script>var bookauthor = \""+getbookauthor+"\"</script>";
            getbookname = "<script>var bookname = \""+getbookname+"\"</script>";
            getbookdate = "<script>var bookdate = \""+getbookdate+"\"</script>";
            getbookprice = "<script>var bookprice = \""+getbookprice+"\"</script>";
            getbookintroduction = "<script>var bookintroduction = \""+getbookintroduction+"\"</script>";
            title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";

            fs = require('fs')
            fs.readFile('bookdata.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(getbookprice+fav+getbookstatus+getbookintroduction+getbookname+getbookauthor+getbookdate+title+data);
            res.end();
            });
          });
    }
})

app.get('/order',function(req,res){
    //var bookdata = "";
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    if(getusertype!="admin"){
        res.redirect('/');
    }else{
        main().catch(console.error);
    }
    
    async function findAll(client) {
        const result = await client.db("order_data").collection("listorder").find({status:"N"}).toArray(function(err, result) {
            if (err) throw err;
            var orderdata = "";
            result.forEach(element => {
                console.log(element);
                orderdata+="<p>Order ID:"+element._id+"</p><p>"+element.bookname+"</p><p>Book Quantity:"+element.bookquantity+"</p><p>Totalprice:"+element.totalprice+"</p><p>Contact Number:"+element.phone+"</p><p>Delivery Address:"+element.address+"</p><p>Delivery Dates:"+element.date
                +"</p><form action=\"dofinishorder\"method=\"POST\" ><button type=\"submit\" class=\"btn btn-primary\" id=\"order\" name=\"order\" value=\""+element.time+"\">Finish Order</button></form>"
                +"<form action=\"dodeleteorder\"method=\"POST\" ><button type=\"submit\" class=\"btn btn-primary\" id=\"order\" name=\"order\" value=\""+element.time+"\">Delete Order</button></form><hr>"
            });
            var orderstatus = "<script>var orderstatus = 'N'</script>";
            orderdata = "<script>var orderdata = '"+orderdata+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('order.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(orderstatus+orderdata+title+data);
            res.end();
    });
          });
    }
})

app.get('/orderY',function(req,res){
    //var bookdata = "";
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    if(getusertype!="admin"){
        res.redirect('/');
    }else{
        main().catch(console.error);
    }
    
    async function findAll(client) {
        const result = await client.db("order_data").collection("listorder").find({status:"Y"}).toArray(function(err, result) {
            if (err) throw err;
            var orderdata = "";
            result.forEach(element => {
                console.log(element);
                orderdata+="<p>Order ID:"+element._id+"</p><p>"+element.bookname+"</p><p>Book Quantity:"+element.bookquantity+"</p><p>Totalprice:"+element.totalprice+"</p><p>Contact Number:"+element.phone+"</p><p>Delivery Address:"+element.address+"</p><p>Delivery Dates:"+element.date
                +"</p><hr>"
            });
            var orderstatus = "<script>var orderstatus = 'Y'</script>";
            orderdata = "<script>var orderdata = '"+orderdata+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('order.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(orderstatus+orderdata+title+data);
            res.end();
    });
          });
    }
})

app.get('/orderF',function(req,res){
    var getname = req.query.bookname;
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();
    
            await findAll(client);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    if(getusertype!="admin"){
        res.redirect('/');
    }else{
        main().catch(console.error);
    }
    
    async function findAll(client) {
        const result = await client.db("order_data").collection("listorder").find({bookname: new RegExp(getname)}).toArray(function(err, result) {
            if (err) throw err;
            var orderdata = "";
            var setstatus = "Pending Order";
            result.forEach(element => {
                console.log(element);
                if(element.status=="N"){
                    setstatus = "Pending Order";
                }else{
                    setstatus = "Complete transaction order"
                }
                orderdata+="<p>Order ID:"+element._id+"</p><p>"+element.bookname+"</p><p>Book Quantity:"+element.bookquantity+"</p><p>Totalprice:"+element.totalprice+"</p><p>Contact Number:"+element.phone+"</p><p>Delivery Address:"+element.address+"</p><p>Delivery Dates:"+element.date
                +"</p><p>Status:"+setstatus+"</p><hr>"
            });
            var orderstatus = "<script>var orderstatus = 'F'</script>";
            orderdata = "<script>var orderdata = '"+orderdata+"'</script>";
            var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
            fs = require('fs')
            fs.readFile('order.html', 'utf8', function (err,data) {
            if (err) {
               return console.log(err);
            }
            //res.sendFile('main.html', { root: '.' })
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(orderstatus+orderdata+title+data);
            res.end();
    });
          });
    }
})

app.post('/buy',function(req,res){
    var buynum = req.body.buynum;
    var buy = req.body.buy;
    var buyarray = buy.split("@@@@$$$$");
    var bookname = buyarray[0];
    var bookprice = buyarray[1];
    buynum = "<script>var buynum = '"+buynum+"'</script>";
    bookname = "<script>var bookname = '"+bookname+"'</script>";
    bookprice = "<script>var bookprice = '"+bookprice+"'</script>";
    var title = "<script>var usertype='"+getusertype+"';var name='"+getusername+"';</script>";
    fs = require('fs')
    fs.readFile('buy.html', 'utf8', function (err,data) {
    if (err) {
        return console.log(err);
    }
    res.writeHead(200,{'Content-Type':'text/html'});
    res.write(buynum+bookname+bookprice+title+data);
    res.end();
    });
})

app.get('/dosetstatus', function(req, res) {
    var getdata = req.query.status;
    var dataarray = getdata.split("$$$$@@@@");
    var getname = dataarray[0];
    var getstatus = dataarray[1];
    //console.log(getstatus);
    if(getstatus=="Y"){
        getstatus = "N";
    }else{
        getstatus = "Y";
    }
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();

            await updateListingByName(client, getname, { bookstatus: getstatus});
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function updateListingByName(client, nameOfListing, updatedListing) {
        const result = await client.db("book_data").collection("listbook")
                            .updateOne({ bookname: nameOfListing }, { $set: updatedListing });
        console.log(`${result.matchedCount} document(s) matched the query criteria.`);
        console.log(`${result.modifiedCount} document(s) was/were updated.`);
        res.redirect('/bookdata?bookname=' + getname);
    }
});

app.post('/dofinishorder', function(req, res) {
    var order = req.body.order;
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();

            await updateListingByName(client, order, { status: "Y"});
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function updateListingByName(client, nameOfListing, updatedListing) {
        const result = await client.db("order_data").collection("listorder")
                            .updateOne({ time: nameOfListing }, { $set: updatedListing });
        console.log(`${result.matchedCount} document(s) matched the query criteria.`);
        console.log(`${result.modifiedCount} document(s) was/were updated.`);
        res.redirect('/order');
    }
});

app.post('/dodeleteorder', function(req, res) {
    var order = req.body.order;
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        
        const client = new MongoClient(uri);
    
        try {
            await client.connect();

            await deleteListings(client, order);
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function deleteListings(client, date) {
        const result = await client.db("order_data").collection("listorder")
            .deleteMany({ time: date });
        console.log(`${result.deletedCount} document(s) was/were deleted.`);
        res.redirect('/order');
    }
});

app.post('/doaddfavorite', function(req, res) {
    var getdata = req.body.addfavorite;
    var getdataarray = getdata.split("@@@@$$$$");
    var bookname = getdataarray[0];
    var fstatus = getdataarray[1];
    //console.log(fstatus);
    if(fstatus=="Y"){
        fstatus = "N";
    }else{
        fstatus = "Y";
    }
    //console.log(fstatus);
    async function main() {
        const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
        const nowdate = new Date(Date.now());
        const date = nowdate.getDate();
        const month = nowdate.getMonth()+1;
        const year = nowdate.getFullYear();
        var todate = year+"-"+month+"-"+date;
        const client = new MongoClient(uri);
    
        try {
            await client.connect();

            //await updateListingByName(client, getname, { bookstatus: getstatus});
            if(fstatus=="Y"){
                await createListing(client,
                {
                  bookname: bookname,
                  useremail: getuseremail,
                  adddate: todate
                }
              );
            }else{
                await deleteListingByName(client);
            }
         
        } catch (e) {
            console.error(e);
        }finally {
            await client.close();
        }
    }
    
    main().catch(console.error);
    
    async function createListing(client, newListing){
        const result = await client.db("favorite_data").collection("listfavorite").insertOne(newListing);
        console.log(`New listing created with the following id: ${result.insertedId}`);
        res.redirect('/bookdata?bookname=' + bookname);
    }
    async function deleteListingByName(client) {
        const result = await client.db("favorite_data").collection("listfavorite")
                .deleteOne({ bookname: bookname,useremail: getuseremail});
        console.log(`${result.deletedCount} document(s) was/were deleted.`);
        res.redirect('/bookdata?bookname=' + bookname);
    }
});

app.post('/doregistered',function(req,res){
    var getname = req.body.name;
    var getemail = req.body.email;
    var getpassword = req.body.password;
    var getrepassword = req.body.repassword;

    async function main() {
	const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    
    const client = new MongoClient(uri,{useNewUrlParser: true,useUnifiedTopology: true});

    try {
        await client.connect();
    
        //await listDatabases(client);

    
            await createListing(client,
                {
                    name: getname,
                    email: getemail,
                    password: getpassword,
                    usertype: "user",
                }
            );
    
     
    } catch (e) {
        console.error(e);
    }finally {
        await client.close();
    }
}

if(getrepassword!=getpassword){
    res.redirect('/registered');
}else{
main().catch(console.error);
}

async function createListing(client, newListing){
    const result = await client.db("user_data").collection("listuser").insertOne(newListing);
    //console.log(`New listing created with the following id: ${result.insertedId}`);
    res.redirect('/tick');
}
})

app.post('/doadminregistered',function(req,res){
    var getname = req.body.name;
        var getemail = req.body.email;
        var getpassword = req.body.password;
        var getrepassword = req.body.repassword;

    async function main() {
	const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    
    const client = new MongoClient(uri,{useNewUrlParser: true,useUnifiedTopology: true});

    try {
        await client.connect();
    
        //await listDatabases(client);

    
            await createListing(client,
                {
                    name: getname,
                    email: getemail,
                    password: getpassword,
                    usertype: "admin",
                }
            );
    
     
    } catch (e) {
        console.error(e);
    }finally {
        await client.close();
    }
}

if(getrepassword!=getpassword){
    res.redirect('/registered');
}else{
main().catch(console.error);
}

async function createListing(client, newListing){
    const result = await client.db("user_data").collection("listuser").insertOne(newListing);
    //console.log(`New listing created with the following id: ${result.insertedId}`);
    res.redirect('/');
}
})

app.post('/dodeletebook',function(req,res){
    var getname = req.body.delete;
    console.log(getname);

    async function main() {
	const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    
    const client = new MongoClient(uri,{useNewUrlParser: true,useUnifiedTopology: true});

    try {
        await client.connect();
    
        await deleteListingByName(client, getname);  

        await deleteListings(client, getname);
     
    } catch (e) {
        console.error(e);
    }finally {
        await client.close();
    }
}

main().catch(console.error);

async function deleteListings(client, date) {
    const result = await client.db("favorite_data").collection("listfavorite")
        .deleteMany({ bookname: date });
    console.log(`${result.deletedCount} document(s) was/were deleted.`);
    res.redirect('/');
}

async function deleteListingByName(client, nameOfListing) {
    const result = await client.db("book_data").collection("listbook")
            .deleteOne({ bookname: nameOfListing });
    console.log(`${result.deletedCount} document(s) was/were deleted.`);
    //res.redirect('/');
}
})

app.post('/dologin',function(req,res){
    var getemail = req.body.email;
    var getpassword = req.body.password;
    async function main() {
	const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    
    const client = new MongoClient(uri);

    try {
        await client.connect();

        await findOneListingByName(client, getemail);
     
    } catch (e) {
        console.error(e);
    }finally {
        await client.close();
    }
}

main().catch(console.error);

async function findOneListingByName(client, nameOfListing) {
    const result = await client.db("user_data").collection("listuser").findOne({ email: nameOfListing });
    if (result) {
        console.log(`Found a listing in the collection with the name '${nameOfListing}':`);
        console.log(result.password);
        if(getpassword==result.password){
            getusername = result.name;
            getuserid = result._id;
            getuseremail = result.email;
            getuserpassword = result.password;
            getusertype = result.usertype;
            res.redirect('/');
        }else{
            res.redirect('/login');
        }
    } else {
        console.log(`No listings found with the name '${nameOfListing}'`);
        res.redirect('/login');
    }
}
})

app.post('/dobuy',function(req,res){
    var bookname = req.body.name;
    var bookquantity = req.body.quantity;
    var totalprice = req.body.price;
    var phone = req.body.phone;
    var address = req.body.address;
    var date = req.body.date;

    async function main() {
	const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    
    const client = new MongoClient(uri,{useNewUrlParser: true,useUnifiedTopology: true});

    try {
        await client.connect();
    
        //await listDatabases(client);

    
            await createListing(client,
                {
                    bookname: bookname,
                    bookquantity: bookquantity,
                    totalprice: totalprice,
                    phone: phone,
                    address: address,
                    date: date,
                    status: "N",
                    time: Date.now()+getuseremail
                }
            );
    
     
    } catch (e) {
        console.error(e);
    }finally {
        await client.close();
    }
}

main().catch(console.error);

async function createListing(client, newListing){
    const result = await client.db("order_data").collection("listorder").insertOne(newListing);
    //console.log(`New listing created with the following id: ${result.insertedId}`);
    res.redirect('/buyfinish');
}
})

app.get('/buyfinish',function(req,res){
    res.sendFile('buyfinish.html', { root: '.' })
})

// app.get('/text', function(req, res) {
//     res.sendFile('text.html', { root: '.' })
//     async function main() {
//         const uri = "mongodb+srv://a58591219:adqk9526@cluster0.h8uny.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
//         const client = new MongoClient(uri);
//         try {
//             await client.connect();
//             await listDatabases(client);
//         } catch (e) {
//             console.error(e);
//         }finally {
//             await client.close();
//         }
//     }
    
//     main().catch(console.error);
    
//     async function listDatabases(client){
//         var text = "";
//         databasesList = await client.db().admin().listDatabases();
     
//         text += "Databases:</br>";
//         databasesList.databases.forEach(db => text += ` - ${db.name}`+"</br>");
//         res.writeHead(200,{'Content-Type':'text/html'});
//         res.write('<html><body>'+text+'</body></html>');
//         res.end();
//     };
// })


const hostname = '127.0.0.1';
const port = 3000;
app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});