const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

// fs.readFile('./index.html', function (err, html) {

//     if (err) throw err;    

//     http.createServer(function(request, response) {  
//         response.writeHeader(200, {"Content-Type": "text/html"});  
//         response.write(html);  
//         response.end();  
//     }).listen(PORT);
// });

//var http = require('http');
// var fs = require('fs');

// var server = http.createServer(function(req, resp){
//   // Print the name of the file for which request is made.
//   console.log("Request for demo file received.");
//   fs.readFile("./index.html",function(error, data){
//     if (error) {
//       resp.writeHead(404);
//       resp.write('Contents you are looking for-not found');
//       resp.end();
//     }  else {
//       resp.writeHead(200, {
//         'Content-Type': 'text/html'
//       });
//       resp.write(data.toString());
//       resp.end();
//     }
//   });
// });

var server=http.createServer(function(req,res){
  if(req.url=='/'){
      res.writeHead(200,{'Content-Type':'text/html'});
      res.write('<html><body>This is Home Page.</body></html>');
      res.end();
  }else if(req.url=='/student'){
      res.writeHead(200,{'Content-Type':'text/html'});
      res.write('<html><body>This is student Page.</body></html>');
      res.end();
  }else if(req.url=='/admin'){
      res.writeHead(200,{'Content-Type':'text/html'});
      res.write('<html><body>This is admin Page.</body></html>');
      res.end();
  }else if (req.url == '/data') { //check the URL of the current request
      fs.readFile("./index.html",function(error, data){
          if (error) {
            res.writeHead(404);
            res.write('Contents you are looking for-not found');
            res.end();
          }  else {
            res.writeHead(200, {
              'Content-Type': 'text/html'
            });
            res.write(data.toString());
            res.end();
          }
        });
  }else
      res.end('Invalid Request!');

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});